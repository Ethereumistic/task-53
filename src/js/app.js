import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  const products = document.querySelectorAll('.product');

  for (const product of products) {
      const priceSpan = product.querySelector('.price');
      const price = priceSpan.textContent;
      product.setAttribute("data-price", price);
  }
});
